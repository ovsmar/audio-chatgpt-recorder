document.getElementById('record').addEventListener('click', () => {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    chrome.scripting.executeScript({
      target: { tabId: tabs[0].id },
      function: recordAudio
    });
  });
});

function recordAudio() {
  const audioElement = document.querySelector('audio');

  if (audioElement) {
    const audioContext = new (window.AudioContext || window.webkitAudioContext)();
    const source = audioContext.createMediaElementSource(audioElement);
    const destination = audioContext.createMediaStreamDestination();
    source.connect(destination);
    source.connect(audioContext.destination);

    const mediaRecorder = new MediaRecorder(destination.stream);
    const chunks = [];

    mediaRecorder.ondataavailable = event => {
      if (event.data.size > 0) {
        chunks.push(event.data);
      }
    };

    mediaRecorder.onstop = () => {
      const blob = new Blob(chunks, { type: 'audio/mp3' });
      const url = URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.style.display = 'none';
      a.href = url;
      a.download = 'recorded_audio.mp3'; // or any other desired file name and extension
      document.body.appendChild(a);
      a.click();
      URL.revokeObjectURL(url);
    };

    audioElement.onended = () => {
      mediaRecorder.stop();
    };

    audioElement.play().then(() => {
      mediaRecorder.start();
    }).catch(error => {
      console.error('Error playing audio:', error);
    });
  } else {
    console.error('Audio element not found');
  }
}
